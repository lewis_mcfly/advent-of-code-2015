add-day:
	mkdir day$(DAY)
	touch day$(DAY)/input
	# puzzle_xx01
	cp dayxx/puzzle_xx01.py day$(DAY)/puzzle_$(DAY)01.py
	sed -e 's/xx/$(DAY)/g' day$(DAY)/puzzle_$(DAY)01.py > day$(DAY)/puzzle_$(DAY)01.py.tmp
	mv day$(DAY)/puzzle_$(DAY)01.py.tmp day$(DAY)/puzzle_$(DAY)01.py
	# puzzle_xx02
	cp dayxx/puzzle_xx02.py day$(DAY)/puzzle_$(DAY)02.py
	sed -e 's/xx/$(DAY)/g' day$(DAY)/puzzle_$(DAY)02.py > day$(DAY)/puzzle_$(DAY)02.py.tmp
	mv day$(DAY)/puzzle_$(DAY)02.py.tmp day$(DAY)/puzzle_$(DAY)02.py
	# test_puzzle_xx01
	cp dayxx/test_puzzle_xx01.py day$(DAY)/test_puzzle_$(DAY)01.py
	sed -e 's/xx/$(DAY)/g' day$(DAY)/test_puzzle_$(DAY)01.py > day$(DAY)/test_puzzle_$(DAY)01.py.tmp
	mv day$(DAY)/test_puzzle_$(DAY)01.py.tmp day$(DAY)/test_puzzle_$(DAY)01.py
	# test_puzzle_xx02
	cp dayxx/test_puzzle_xx02.py day$(DAY)/test_puzzle_$(DAY)02.py
	sed -e 's/xx/$(DAY)/g' day$(DAY)/test_puzzle_$(DAY)02.py > day$(DAY)/test_puzzle_$(DAY)02.py.tmp
	mv day$(DAY)/test_puzzle_$(DAY)02.py.tmp day$(DAY)/test_puzzle_$(DAY)02.py
