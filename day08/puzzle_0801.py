import re

hex_re = re.compile(r"\\x[0-9a-f][0-9a-f]")
quote_re = re.compile(r"\\\"")
slash_re = re.compile(r"\\\\")

def code_size(string):
    return len(string)

def memory_size(string):
    parsed = str(string)
    parsed = parsed.strip('"')
    parsed = slash_re.sub(r"_", parsed)
    parsed = quote_re.sub('"', parsed)
    parsed = hex_re.sub("?", parsed)
    parsed = parsed.replace("_", '"')
    parsed = parsed.strip(" ")

    return len(parsed)

if __name__ == '__main__':
    with open('day08/input') as input_file:
        strings = input_file.read().splitlines()
        code_size = sum([code_size(string) for string in strings])
        memory_size = sum([memory_size(string) for string in strings])
        print(f"{code_size=} {memory_size=} {(code_size - memory_size)=}")
