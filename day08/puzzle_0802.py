import puzzle_0801 as p1

def encoded_size(string):
    encoded = string.replace('\\', '\\\\')
    encoded = encoded.replace('"', '\\\"')
    encoded = '"' + encoded + '"'
    return len(encoded)

if __name__ == '__main__':
    with open('day08/input') as input_file:
        strings = input_file.read().splitlines()
        code_size = sum([p1.code_size(string) for string in strings])
        encoded_size = sum([encoded_size(string) for string in strings])
        print(f"{code_size=} {encoded_size=} {(encoded_size - code_size)=}")
