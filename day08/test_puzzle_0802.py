import puzzle_0802 as p

test_values = None
with open('day08/input_test') as input_file: 
    # ["", "abc", "aaa\"aaa", "\x27"]
    test_values = input_file.read().splitlines()

def test_encoded_size():
    assert p.encoded_size(test_values[0]) == 6
    assert p.encoded_size(test_values[1]) == 9
    assert p.encoded_size(test_values[2]) == 16
    assert p.encoded_size(test_values[3]) == 11
