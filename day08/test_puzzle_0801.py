import ast
import puzzle_0801 as p

test_values = None
with open('day08/input_test') as input_file: 
    # ["", "abc", "aaa\"aaa", "\x27"]
    test_values = input_file.read().splitlines()

def test_code_size():
    assert p.code_size(test_values[0]) == 2
    assert p.code_size(test_values[1]) == 5
    assert p.code_size(test_values[2]) == 10
    assert p.code_size(test_values[3]) == 6
    assert p.code_size(test_values[4]) == 28

def test_memory_size():
    assert p.memory_size(test_values[0]) == 0
    assert p.memory_size(test_values[1]) == 3
    assert p.memory_size(test_values[2]) == 7
    assert p.memory_size(test_values[3]) == 1
    assert p.memory_size(test_values[4]) == 18

def test_code_size_sum():
    code_size = sum([p.code_size(string) for string in test_values[:4]])
    memory_size = sum([p.memory_size(string) for string in test_values[:4]])
    assert code_size == 23
    assert memory_size == 11
    assert code_size - memory_size == 12
