import re
import string
from dataclasses import dataclass
from functools import cmp_to_key

@dataclass
class Instruction:
    x: 'typing.Any' = None
    x_raw: bool = None
    y: 'typing.Any' = None
    y_raw: bool = None
    operator: str = None
    signal: str = None
    result: int = None

def parse_instruction(instruction):
    pattern = re.compile(r"(?P<in>((?P<x>(?P<x_int>\d+)|(?P<x_alpha>[a-z]+))\s)?(((?P<operator>AND|OR|LSHIFT|RSHIFT|NOT)\s)?(?P<y>(?P<y_int>\d+)|(?P<y_alpha>[a-z]+)))) -> (?P<signal>[a-z]+)")
    match = pattern.match(instruction)
    if not match:
        raise ValueError('Instruction does not match the pattern')
    match = match.groupdict()
    result = Instruction()
    if match['x']:
        result.x = match['x'] if match['x_alpha'] else int(match['x'])
        result.x_raw = match['x_int'] is not None
    if match['y']:
        result.y = match['y'] if match['y_alpha'] else int(match['y'])
        result.y_raw = match['y_int'] is not None
    if match['operator']:
        result.operator = match['operator'].lower()
    result.signal = match['signal']
    return result

def compute_instruction(signals, instruction):
    if instruction.result:
        return instruction.result
    if instruction.x is not None:
        x = instruction.x if instruction.x_raw else compute_instruction(signals, signals[instruction.x])
    y = instruction.y if instruction.y_raw else compute_instruction(signals, signals[instruction.y])
    result = None
    if instruction.operator == 'and':
        result = x & y
    elif instruction.operator == 'or':
        result = x | y
    elif instruction.operator == 'not':
        result = 0b1111111111111111 - y
    elif instruction.operator == 'lshift':
        result = x << y
    elif instruction.operator == 'rshift':
        result = x >> y
    elif instruction.operator is None:
        result = y
    instruction.result = result
    return result

def compute_signal(instructions, signal):
    signals = dict()
    for instruction in instructions:
        signals[instruction.signal] = instruction
    return compute_instruction(signals, signals[signal])


if __name__ == '__main__':
    with open('day07/input') as input_file:
        instructions = [parse_instruction(line) for line in input_file.read().splitlines()]
        print(f"{compute_signal(instructions, 'a')=}")
