import puzzle_0701

def test_parse_instruction():
    assert puzzle_0701.parse_instruction('123 -> x') == puzzle_0701.Instruction(None, None, 123, True, None, 'x')
    assert puzzle_0701.parse_instruction('456 -> y') == puzzle_0701.Instruction(None, None, 456, True, None, 'y')
    assert puzzle_0701.parse_instruction('x AND y -> d') == puzzle_0701.Instruction('x', False, 'y', False, 'and', 'd')
    assert puzzle_0701.parse_instruction('x OR y -> e') == puzzle_0701.Instruction('x', False, 'y', False, 'or', 'e')
    assert puzzle_0701.parse_instruction('x LSHIFT 2 -> f') == puzzle_0701.Instruction('x', False, 2, True, 'lshift', 'f')
    assert puzzle_0701.parse_instruction('y RSHIFT 2 -> g') == puzzle_0701.Instruction('y', False, 2, True, 'rshift', 'g')
    assert puzzle_0701.parse_instruction('NOT x -> h') == puzzle_0701.Instruction(None, None, 'x', False, 'not', 'h')
    assert puzzle_0701.parse_instruction('NOT y -> i') == puzzle_0701.Instruction(None, None, 'y', False, 'not', 'i')

def test_compute_signal():
    instructions = [
        puzzle_0701.Instruction(None, None, 123, True, None, 'x'),
        puzzle_0701.Instruction(None, None, 456, True, None, 'y'),
        puzzle_0701.Instruction('x', False, 'y', False, 'and', 'd'),
        puzzle_0701.Instruction('x', False, 'y', False, 'or', 'e'),
        puzzle_0701.Instruction('x', False, 2, True, 'lshift', 'f'),
        puzzle_0701.Instruction('y', False, 2, True, 'rshift', 'g'),
        puzzle_0701.Instruction(None, None, 'x', False, 'not', 'h'),
        puzzle_0701.Instruction(None, None, 'y', False, 'not', 'i')
    ]
    assert puzzle_0701.compute_signal(instructions, 'x') == 123
    assert puzzle_0701.compute_signal(instructions, 'y') == 456
    assert puzzle_0701.compute_signal(instructions, 'd') == 72
    assert puzzle_0701.compute_signal(instructions, 'e') == 507
    assert puzzle_0701.compute_signal(instructions, 'f') == 492
    assert puzzle_0701.compute_signal(instructions, 'g') == 114
    assert puzzle_0701.compute_signal(instructions, 'h') == 65412
    assert puzzle_0701.compute_signal(instructions, 'i') == 65079
