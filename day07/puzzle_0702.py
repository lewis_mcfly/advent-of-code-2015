import puzzle_0701

def compute_signal(signals, instructions, signal):
    for instruction in instructions:
        if instruction.signal not in signals:
            signals[instruction.signal] = instruction
    return puzzle_0701.compute_instruction(signals, signals[signal])

if __name__ == '__main__':
    with open('day07/input') as input_file:
        instructions = [puzzle_0701.parse_instruction(line) for line in input_file.read().splitlines()]
        a = compute_signal(dict(), instructions, 'a')
    with open('day07/input') as input_file:
        instructions = [puzzle_0701.parse_instruction(line) for line in input_file.read().splitlines()]
        a = compute_signal({'b': puzzle_0701.Instruction(None, None, a, True, None, 'b', a)}, instructions, 'a')
        print(f"{a=}")
