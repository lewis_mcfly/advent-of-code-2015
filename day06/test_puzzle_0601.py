import puzzle_0601

def test_parse_instruction():
    assert puzzle_0601.parse_instruction('turn on 660,55 through 986,197') == ('on', (660, 55), (986, 197))
    assert puzzle_0601.parse_instruction('turn off 660,55 through 986,197') == ('off', (660, 55), (986, 197))
    assert puzzle_0601.parse_instruction('turn off 1,2 through 23,23') == ('off', (1, 2), (23, 23))
    assert puzzle_0601.parse_instruction('toggle 1,2 through 23,23') == ('toggle', (1, 2), (23, 23))
