import puzzle_0601

if __name__ == '__main__':
    with open('day06/input') as input_file:
        grid = [[0 for i in range(1000)] for j in range(1000)]
        instructions = [puzzle_0601.parse_instruction(line) for line in input_file.read().splitlines()]
        for operation, bottom_left, top_right in instructions:
            x0, y0 = bottom_left
            x1, y1 = top_right
            for x in range(x0, x1 + 1):
                for y in range(y0, y1 + 1):
                    if operation == 'on':
                        grid[x][y] += 1
                    elif operation == 'off':
                        grid[x][y] = max(0, grid[x][y] - 1)
                    elif operation == 'toggle':
                        grid[x][y] += 2
                    else:
                        raise ValueError('Operation should be "on", "off" or "toggle"')

        result = 0
        for line in grid:
            result += sum(line)
        print(f"{result=}")