import re
from functools import reduce

def parse_instruction(instruction):
    pattern = re.compile(r"(turn on|turn off|toggle) (\d{1,3}),(\d{1,3}) through (\d{1,3}),(\d{1,3})")
    match = pattern.match(instruction)
    if len(match.groups()) < 5:
        raise ValueError('Instruction does not match the pattern')
    operation = 'on' if match.group(1) == 'turn on' else 'off' if match.group(1) == 'turn off' else 'toggle'
    return (operation, (int(match.group(2)), int(match.group(3))), (int(match.group(4)), int(match.group(5))))

if __name__ == '__main__':
    with open('day06/input') as input_file:
        grid = [[0 for i in range(1000)] for j in range(1000)]
        instructions = [parse_instruction(line) for line in input_file.read().splitlines()]
        for operation, bottom_left, top_right in instructions:
            x0, y0 = bottom_left
            x1, y1 = top_right
            for x in range(x0, x1 + 1):
                for y in range(y0, y1 + 1):
                    if operation == 'on':
                        grid[x][y] = 1
                    elif operation == 'off':
                        grid[x][y] = 0
                    elif operation == 'toggle':
                        grid[x][y] = 1 if grid[x][y] == 0 else 0
                    else:
                        raise ValueError('Operation should be "on", "off" or "toggle"')

        result = 0
        for line in grid:
            result += sum(line)
        print(f"{result=}")