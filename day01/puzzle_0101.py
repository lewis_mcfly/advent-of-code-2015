from functools import reduce

def get_floor(instructions):
    operations = [1 if instruction == '(' else -1 for instruction in instructions]
    return reduce(lambda x, y: x + y, operations)

if __name__ == '__main__':
    with open('day01/input') as input_file:
        instructions = input_file.read().splitlines()[0]
        floor = get_floor(instructions)
        print(f"{floor=}")
