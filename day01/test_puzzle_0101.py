import puzzle_0101

def test_get_floor():
    assert 0 == puzzle_0101.get_floor('(())')
    assert 0 == puzzle_0101.get_floor('()()')
    assert 3 == puzzle_0101.get_floor('(((')
    assert 3 == puzzle_0101.get_floor('(()(()(')
    assert 3 == puzzle_0101.get_floor('))(((((')
    assert -1 == puzzle_0101.get_floor('())')
    assert -1 == puzzle_0101.get_floor('))(')
    assert -3 == puzzle_0101.get_floor(')))')
    assert -3 == puzzle_0101.get_floor(')())())')
