from functools import reduce

def get_basement_index(instructions):
    operations = [1 if instruction == '(' else -1 for instruction in instructions]
    floor = 0
    for index, operation in enumerate(operations):
        floor += operation
        if floor == -1:
            return index + 1
    raise ValueError('-1 floor was not found')

if __name__ == '__main__':
    with open('day01/input') as input_file:
        instructions = input_file.read().splitlines()[0]
        index = get_basement_index(instructions)
        print(f"{index=}")
