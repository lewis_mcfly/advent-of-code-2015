import puzzle_0102

def test_get_basement_index():
    assert 1 == puzzle_0102.get_basement_index(')')
    assert 5 == puzzle_0102.get_basement_index('()())')
