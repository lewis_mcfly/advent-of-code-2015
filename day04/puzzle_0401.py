import hashlib 

def check_md5(key, value):
    md5_hash = hashlib.md5((str(key) + str(value)).encode()).hexdigest()
    return md5_hash.startswith('00000')

if __name__ == '__main__':
    result = 0
    while not check_md5('ckczppom', result):
        if result > 1000000:
            raise ValueError(f"Result is too high ({result})")
        result += 1
    print(f"{result=}")
