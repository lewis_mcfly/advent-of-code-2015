import puzzle_0401

def test_check_md5():
    assert puzzle_0401.check_md5('abcdef', 609043) == True
    assert puzzle_0401.check_md5('abcdef', 609042) == False
