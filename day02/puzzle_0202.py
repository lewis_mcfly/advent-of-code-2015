import puzzle_0201

def minimum_perimeter(l, w, h):
    return min(2*(l+w), 2*(w+h), 2*(h+l))

def ribbon(l, w, h):
    return minimum_perimeter(l, w, h) + (l*w*h)

if __name__ == '__main__':
    with open('day02/input') as input_file:
        dimensions = input_file.read().splitlines()
        dimensions = [dimensions_string.split('x') for dimensions_string in dimensions]
        dimensions = [(int(dimension_array[0]), int(dimension_array[1]), int(dimension_array[2])) for dimension_array in dimensions]
        result = sum([ribbon(*dimension) for dimension in dimensions])
        print(f"{result=}")