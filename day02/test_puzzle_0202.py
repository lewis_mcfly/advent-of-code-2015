import puzzle_0202

def test_minimum_perimeter():
    assert puzzle_0202.minimum_perimeter(2, 3, 4) == 10
    assert puzzle_0202.minimum_perimeter(1, 1, 10) == 4

def test_ribbon():
    assert puzzle_0202.ribbon(2, 3, 4) == 34
    assert puzzle_0202.ribbon(1, 1, 10) == 14