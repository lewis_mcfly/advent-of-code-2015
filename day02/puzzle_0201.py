def surface(l, w, h):
    return 2*l*w + 2*w*h + 2*h*l

def wrapping_paper_quantity(l, w, h):
    return surface(l, w, h) + min(l*w, w*h, h*l)

if __name__ == '__main__':
    with open('day02/input') as input_file:
        dimensions = input_file.read().splitlines()
        dimensions = [dimensions_string.split('x') for dimensions_string in dimensions]
        dimensions = [(int(dimension_array[0]), int(dimension_array[1]), int(dimension_array[2])) for dimension_array in dimensions]
        result = sum([wrapping_paper_quantity(*dimension) for dimension in dimensions])
        print(f"{result=}")