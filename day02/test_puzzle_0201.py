import puzzle_0201

def test_surface():
    assert puzzle_0201.surface(2, 3, 4) == 52
    assert puzzle_0201.surface(1, 1, 10) == 42

def test_wrapping_paper_quantity():
    assert puzzle_0201.wrapping_paper_quantity(2, 3, 4) == 58
    assert puzzle_0201.wrapping_paper_quantity(1, 1, 10) == 43
