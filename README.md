[![pipeline status](https://gitlab.com/lewis_mcfly/advent-of-code-2015/badges/master/pipeline.svg)](https://gitlab.com/lewis_mcfly/advent-of-code-2015/commits/master)
[![coverage report](https://gitlab.com/lewis_mcfly/advent-of-code-2015/badges/master/coverage.svg)](https://gitlab.com/lewis_mcfly/advent-of-code-2015/commits/master)

# Advent of Code
## 2015

[https://adventofcode.com/2015](https://adventofcode.com/2015)