from dataclasses import dataclass

@dataclass
class Point:
    x: int
    y: int

def move_point(point, operation):
    if operation == '^':
        return Point(point.x, point.y + 1)
    elif operation == '>':
        return Point(point.x + 1, point.y)
    elif operation == 'v':
        return Point(point.x, point.y - 1)
    elif operation == '<':
        return Point(point.x - 1, point.y)
    raise ValueError('Operation should be ^, >, v or <')

def visited_points(operations):
    current_point = Point(0, 0)
    visited_points = dict()
    visited_points[(current_point.x, current_point.y)] = 1
    for operation in operations:
        current_point = move_point(current_point, operation)
        point_tuple = (current_point.x, current_point.y)
        if point_tuple in visited_points:
            visited_points[point_tuple] += 1
        else:
            visited_points[point_tuple] = 0
    return visited_points

if __name__ == '__main__':
    with open('day03/input') as input_file:
        operations = input_file.read().splitlines()[0]
        visited_points = visited_points(operations)
        print(f"{(len(visited_points.keys()))=}")