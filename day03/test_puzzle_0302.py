import puzzle_0302

def test_visited_points():
    assert len(puzzle_0302.visited_points('^v').keys()) == 3
    assert len(puzzle_0302.visited_points('^>v<').keys()) == 3
    assert len(puzzle_0302.visited_points('^v^v^v^v^v').keys()) == 11
