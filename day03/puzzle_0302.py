import puzzle_0301

def visited_points(operations):
    robot_current_point = puzzle_0301.Point(0, 0)
    santa_current_point = puzzle_0301.Point(0, 0)
    visited_points = dict()
    visited_points[(robot_current_point.x, robot_current_point.y)] = 2

    for index, operation in enumerate(operations):
        if index % 2 == 0:
            robot_current_point = puzzle_0301.move_point(robot_current_point, operation)
            point_tuple = (robot_current_point.x, robot_current_point.y)
        else:
            santa_current_point = puzzle_0301.move_point(santa_current_point, operation)
            point_tuple = (santa_current_point.x, santa_current_point.y)

        if point_tuple in visited_points:
            visited_points[point_tuple] += 1
        else:
            visited_points[point_tuple] = 0
    return visited_points

if __name__ == '__main__':
    with open('day03/input') as input_file:
        operations = input_file.read().splitlines()[0]
        visited_points = visited_points(operations)
        print(f"{(len(visited_points.keys()))=}")