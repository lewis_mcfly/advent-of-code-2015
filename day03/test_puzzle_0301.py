import puzzle_0301

def test_move_point():
    assert puzzle_0301.Point(0, 1) == puzzle_0301.move_point(puzzle_0301.Point(0, 0), '^')
    assert puzzle_0301.Point(1, 0) == puzzle_0301.move_point(puzzle_0301.Point(0, 0), '>')
    assert puzzle_0301.Point(0, -1) == puzzle_0301.move_point(puzzle_0301.Point(0, 0), 'v')
    assert puzzle_0301.Point(-1, 0) == puzzle_0301.move_point(puzzle_0301.Point(0, 0), '<')

def test_visited_points():
    assert len(puzzle_0301.visited_points('>').keys()) == 2
    assert len(puzzle_0301.visited_points('^>v<').keys()) == 4
    assert len(puzzle_0301.visited_points('^v^v^v^v^v').keys()) == 2