import puzzle_0501

def test_has_three_voyels():
    assert puzzle_0501.has_three_voyels('aei') == True
    assert puzzle_0501.has_three_voyels('xazegov') == True
    assert puzzle_0501.has_three_voyels('aeiouaeiouaeiou') == True
    assert puzzle_0501.has_three_voyels('erty') == False
    assert puzzle_0501.has_three_voyels('azer') == False
    assert puzzle_0501.has_three_voyels('') == False
    assert puzzle_0501.has_three_voyels('ae') == False

def test_has_double_letter():
    assert puzzle_0501.has_double_letter('aa') == True
    assert puzzle_0501.has_double_letter('zz') == True
    assert puzzle_0501.has_double_letter('zze') == True
    assert puzzle_0501.has_double_letter('zzee') == True
    assert puzzle_0501.has_double_letter('azertyy') == True
    assert puzzle_0501.has_double_letter('azerrty') == True
    assert puzzle_0501.has_double_letter('aazerrty') == True
    assert puzzle_0501.has_double_letter('azerty') == False
    assert puzzle_0501.has_double_letter('azertya') == False
    assert puzzle_0501.has_double_letter('') == False

def test_has_forbidden_string():
    assert puzzle_0501.has_forbidden_strings('ab') == True
    assert puzzle_0501.has_forbidden_strings('cd') == True
    assert puzzle_0501.has_forbidden_strings('pq') == True
    assert puzzle_0501.has_forbidden_strings('xy') == True
    assert puzzle_0501.has_forbidden_strings('abpq') == True
    assert puzzle_0501.has_forbidden_strings('abab') == True
    assert puzzle_0501.has_forbidden_strings('abrf') == True
    assert puzzle_0501.has_forbidden_strings('') == False
    assert puzzle_0501.has_forbidden_strings('a') == False
    assert puzzle_0501.has_forbidden_strings('azerty') == False

def test_is_nice():
    assert puzzle_0501.is_nice('ugknbfddgicrmopn') == True
    assert puzzle_0501.is_nice('aaa') == True
    assert puzzle_0501.is_nice('jchzalrnumimnmhp') == False
    assert puzzle_0501.is_nice('haegwjzuvuyypxyu') == False
    assert puzzle_0501.is_nice('dvszwmarrgswjxmb') == False
