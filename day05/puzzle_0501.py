import re
import string

def has_three_voyels(string_input):
    total = 0
    for voyel in 'aeiou':
        total += string_input.count(voyel)
        if total >= 3:
            return True
    return False

def has_double_letter(string_input):
    for character in string.ascii_lowercase:
        if 2*character in string_input:
            return True
    return False

def has_forbidden_strings(string_input):
    for forbidden_string in ['ab', 'cd', 'pq', 'xy']:
        if forbidden_string in string_input:
            return True
    return False

def is_nice(string_input):
    return has_three_voyels(string_input) and has_double_letter(string_input) and not has_forbidden_strings(string_input)

if __name__ == '__main__':
    with open('day05/input') as input_file:
        strings = input_file.read().splitlines()
        nice_strings = [s for s in strings if is_nice(s)]
        print(f"{len(nice_strings)}")
