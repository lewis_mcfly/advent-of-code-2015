import itertools
import string
import re

def has_double_pair(string_input):
    pairs = [x + y for x, y in itertools.product(string.ascii_lowercase, string.ascii_lowercase)]
    for pair in pairs:
        if (position := string_input.find(pair)) >= 0 and string_input.count(pair) >= 2:
            substring = string_input[:position] + string_input[position + 2:]
            if pair in substring:
                return True
    return False

def has_one_between_pair(string_input):
    for pair_character in string.ascii_lowercase:
        for in_between_character in string.ascii_lowercase:
            if pair_character + in_between_character + pair_character in string_input:
                return True
    return False

def is_nice(string_input):
    return has_double_pair(string_input) and has_one_between_pair(string_input)

if __name__ == '__main__':
    with open('day05/input') as input_file:
        strings = input_file.read().splitlines()
        nice_strings = [s for s in strings if is_nice(s)]
        print(nice_strings)
        print(f"{len(nice_strings)}")
