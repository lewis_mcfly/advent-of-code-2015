import puzzle_0502

def test_has_double_pair():
    assert puzzle_0502.has_double_pair('aaaa') == True
    assert puzzle_0502.has_double_pair('aabaa') == True
    assert puzzle_0502.has_double_pair('xyxy') == True
    assert puzzle_0502.has_double_pair('xybxy') == True
    assert puzzle_0502.has_double_pair('aaaaa') == True
    assert puzzle_0502.has_double_pair('qjhvhtzxzqqjkmpb') == True
    assert puzzle_0502.has_double_pair('aaa') == False
    assert puzzle_0502.has_double_pair('aa') == False
    assert puzzle_0502.has_double_pair('a') == False
    assert puzzle_0502.has_double_pair('') == False
    assert puzzle_0502.has_double_pair('mmgg') == False

def test_has_one_between_pair():
    assert puzzle_0502.has_one_between_pair('xyx') == True
    assert puzzle_0502.has_one_between_pair('xyxy') == True
    assert puzzle_0502.has_one_between_pair('abcdefeghi') == True
    assert puzzle_0502.has_one_between_pair('aaa') == True
    assert puzzle_0502.has_one_between_pair('qjhvhtzxzqqjkmpb') == True
    assert puzzle_0502.has_one_between_pair('aa') == False
    assert puzzle_0502.has_one_between_pair('') == False
    assert puzzle_0502.has_one_between_pair('xyz') == False

def test_is_nice():
    assert puzzle_0502.is_nice('qjhvhtzxzqqjkmpb') == True
    assert puzzle_0502.is_nice('xxyxx') == True
    assert puzzle_0502.is_nice('uurcxstgmygtbstg') == False
    assert puzzle_0502.is_nice('ieodomkazucvgmuy') == False
    assert puzzle_0502.is_nice('mmggfwapsetemiuj') == False
    assert puzzle_0502.is_nice('ylpxlkcnenbxxtta') == False
    assert puzzle_0502.is_nice('btamaihdivrhhrrv') == False
    assert puzzle_0502.is_nice('elrlnndorggmmhmx') == False
